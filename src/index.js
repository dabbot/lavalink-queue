const http = require('http');

const Redis = require('ioredis');

const v1 = require('./v1');

const REDIS_DATABASE = parseInt(process.env['REDIS_DATABASE'] || 0);
const REDIS_HOST = process.env['REDIS_HOST'] || '0.0.0.0';
const REDIS_PASSWORD = process.env['REDIS_PASSWORD'];
const REDIS_PORT = parseInt(process.env['REDIS_PORT'] || 6379);
const SERVER_PORT = parseInt(process.env['SERVER_PORT'] || 80);

const redis = new Redis({
    db: REDIS_DATABASE,
    host: REDIS_HOST,
    port: REDIS_PORT,
});

const ACTIONS = {
    'V1_GUILDS_INDEX': v1.guilds.index,
    'V1_GUILDS_ADD': v1.guilds.add,
    'V1_GUILDS_DELETE': v1.guilds.del,
    'V1_GUILDS_DELETE_QUEUE': v1.guilds.delete_queue,
    'V1_GUILDS_GET': v1.guilds.get,
    'V1_GUILDS_POP': v1.guilds.pop,
    'V1_GUILDS_SHUFFLE': v1.guilds.shuffle,
};

async function handleRequest(req, res, payload) {
    const string = Buffer.concat(payload).toString();
    const body = JSON.parse(string);

    const call = body['call'];
    const fn = ACTIONS[call];

    if (typeof fn === 'undefined') {
        res.statusCode = 404;

        console.log(`Didn't find ${call}`, ACTIONS);
        res.end('RPC function does not exist.');

        return;
    }

    try {
        const [status, responseBody] = await fn(body['data'], redis);

        res.statusCode = status;
        res.end(responseBody);
    } catch (e) {
        console.log(e);

        res.statusCode = 500;
        res.end('Internal error.');
    }
}

http.createServer((req, res) => {
    const payload = [];

    req.on('data', (chunk) => {
        payload.push(chunk);
    });

    req.on('end', () => {
        handleRequest(req, res, payload);
    });
}).listen(SERVER_PORT);

console.log('Listening on port', SERVER_PORT);
