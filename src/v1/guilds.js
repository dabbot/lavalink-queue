const uuidv4 = require('uuid/v4');

const gen = require('../gen');

async function getOrCreateSong(redis, track) {
    const trackKey = gen.trackSongId(track);
    const id = await redis.get(trackKey);

    if (id) {
        return id;
    }

    const newId = uuidv4();

    const pipeline = redis.pipeline();
    redis.set(trackKey, newId);
    redis.set(gen.songTrackId(newId), track);
    await pipeline.exec();

    return newId;
}

async function replaceQueue(redis, key, newQueueItems) {
    const pipeline = redis.pipeline();
    pipeline.del(key);
    pipeline.rpush(key, newQueueItems);
    await pipeline.exec();
}

function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));

        [array[i], array[j]] = [array[j], array[i]];
    }

    return array;
}

async function add(body, redis) {
    const { guild_id: guildId, track } = body;
    const songId = await getOrCreateSong(redis, track);
    await redis.rpush(gen.guildQueue(guildId), songId);

    return [201, JSON.stringify({
        'id': songId,
    })];
}

async function del(body, redis) {
    const { guild_id: guildId, index } = body;

    const key = gen.guildQueue(guildId);

    let items = await redis.lrange(key, 0, -1);
    items.splice(index, 1);

    await replaceQueue(redis, key, items);

    return [201, JSON.stringify(items)];
}

async function delete_queue(body, redis) {
    await redis.del(gen.guildQueue(body['guild_id']));

    return [204, ''];
}

async function get(body, redis) {
    let { guild_id: guildId, limit, offset } = body;

    offset = offset || 0;
    limit = typeof limit === 'undefined' ? offset + 9 : offset + limit - 1;

    if (limit === 0) {
        return [200, '[]'];
    }

    const key = gen.guildQueue(guildId);
    let ids = await redis.lrange(key, offset, limit);

    if (ids.length === 0) {
        return [200, '[]'];
    }

    const tracks = await redis.mget(ids.map(gen.songTrackId));

    return [200, JSON.stringify(tracks)];
}

function index() {
    return [200, 'Hello'];
}

async function pop(body, redis) {
    const { guild_id: guildId} = body;
    const key = gen.guildQueue(guildId);
    const songId = await redis.lpop(key);

    const track = await redis.get(gen.songTrackId(songId));

    return [201, JSON.stringify(track)];
}

async function shuffle(body, redis) {
    const { guild_id: guildId } = body;

    const key = gen.guildQueue(guildId);

    const items = await redis.lrange(key, 0, -1);
    const shuffled = shuffleArray(items);

    await replaceQueue(key, key, shuffled);

    return [200, JSON.stringify(shuffled)];
}

module.exports = {
    add,
    del,
    delete_queue,
    get,
    index,
    pop,
    shuffle,
};
